import cv2
import torch
from torchvision.models.detection import fasterrcnn_resnet50_fpn
from torchvision.transforms import functional as F

# Load the pre-trained model
model = fasterrcnn_resnet50_fpn(pretrained=True)
model.eval()  # Set the model to evaluation mode

# Function to perform object detection
def detect_objects(frame, model):
    # Convert frame to tensor
    image_tensor = F.to_tensor(frame).unsqueeze(0)
    
    # If using a CUDA-capable GPU, move data to GPU
    if torch.cuda.is_available():
        image_tensor = image_tensor.to('cuda')
        model.to('cuda')
    
    with torch.no_grad():
        predictions = model(image_tensor)
    
    return predictions

# Open the default webcam
cap = cv2.VideoCapture(0)  # Change '0' to '-1' if you encounter any issues with webcam usage

face_detected = False

while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        break
    
    # Detect objects in the frame
    predictions = detect_objects(frame, model)

    # Initialize face detection flag
    face_detected = False

    # Draw bounding boxes and labels on the frame
    for element in range(len(predictions[0]['boxes'])):
        boxes = predictions[0]['boxes'][element].cpu().numpy()
        labels = predictions[0]['labels'][element].cpu().numpy()
        score = predictions[0]['scores'][element].cpu().numpy()
        
        if score > 0.9 and labels == 1:  # Check for 'person' label which is 1
            x1, y1, x2, y2 = boxes.astype(int)
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0, 255, 0), 2)
            cv2.putText(frame, 'Face Detected', (x1, y1-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
            face_detected = True
    
    # Print detection status
    if face_detected:
        print(1)
    else:
        print(0)
    
    # Display the frame
    cv2.imshow('Object Detection', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release resources
cap.release()
cv2.destroyAllWindows()
